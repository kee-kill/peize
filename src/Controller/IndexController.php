<?php
/**
 * Created by PhpStorm.
 * Authon: akio <medue8@gmail.com>
 * Date: 7/29/19
 * Time: 11:08
 */

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class IndexController extends AbstractController
{

    public function index()
    {
        return $this->render('index/index.html.twig', [
            'number' => 111,
        ]);
    }
}